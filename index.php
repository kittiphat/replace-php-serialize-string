<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Replace PHP Serialize String</title>
        <meta name="description" content="An online tool to help you replace PHP Serialized String (Helps a lot in migrating wordpress) written in Javascript.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Kittiphat (Peat) Srilomsak">
	
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
		<style type="text/css">
			html, body {
				height: 100%;
			}
			.onlinetool [class^="col-"] {
				padding-top: 10px;
				padding-bottom: 10px;
			}
			#wrap {
				min-height: 100%;
				height: auto;
				margin: 0 auto -60px;
				padding: 0 0 60px;
			}
			#footer {
				height: 60px;
				background: #f9f9f9;
			}
			#footer .credit {
				text-align: center;
				margin: 20px 0;
			}
		</style>
	</head>
	<body>
		<div id="wrap">
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand"><span class="glyphicon glyphicon-wrench"></span> Replace PHP Serialize String</a>
					</div>
				</div>
			</div>
			<div class="jumbotron">
				<div class="container">
					<h1>/Utility</h1>
					<p>Utility for search and replace a PHP serialized strings via JavaScript. Simply copy-and-paste.</p>
				</div>
			</div>

			<div class="container">
				<p>This tool will help you ease your pain replacing server URL on <a href="//dev.mysql.com/doc/refman/5.1/en/mysqldump.html" target="_blank">MySQL dump</a> files, during <a href="//wordpress.org/" target="_blank">Wordpress</a> migration process.</p>
				<p>This tool is not actually limited to Wordpress, it actually is useful in replacing any <a href="//php.net/manual/en/function.serialize.php" target="_blank">PHP Serialized</a> strings.</p>
			</div>

			<div class="container onlinetool">
				<h3>Online Tool</h3>
				<p>Input (Source) is case sensitive, and Output data is updated everytime Input lose its focus.</p>
				<form role="form">
					<div class="row replacements">
						<div class="form-group">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon indexbox">1</span>
									<input type="text" class="form-control searchfor" placeholder="search for"></input>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></span>
									<input type="text" class="form-control replacewith" placeholder="replace with"></input>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<textarea id="source" class="form-control" rows="12" placeholder="source"></textarea>
						</div>
						<div class="col-md-6">
							<textarea id="output" class="form-control" rows="12" placeholder="output"></textarea>
						</div>
						<div class="clearfix"></div>
					</div>
					<hr/>
				</form>
			</div>
		</div>
		<div id="footer">
			<div class="container">
				<p class="text-muted credit">
					Written and tested on <a href="//www.google.com/chrome/" target="_blank">Chrome</a> 
					by <a href="//about.me/peat/" target="_blank">Peat</a> @ <a href="//www.syllistudio.com/" target="_blank">Sylli Studio</a>
					- embarked with (CDN) <a href="//getbootstrap.com/" target="_blank">Bootstrap 3.0.2</a> 
					+ <a href="//jquery.org/" target="_blank">jQuery 1.10.2</a>
				</p>
			</div>
		</div>

		<!-- Scripts -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			var replacementBlock;

			function getReplacements() {
				var replacements = [],
					duplicateIndex = {};
				$('.replacements').each(function() {
					var searchfor = $(this).find('.searchfor').val(),
						replacewith = $(this).find('.replacewith').val();

					if (searchfor) {
						replacements.push({
							'searchfor' : searchfor,
							'replacewith' : replacewith,
							'$self' : $(this)
						});

						// check duplicate index.
						if (duplicateIndex[searchfor]) {
							duplicateIndex[searchfor].push($(this));
						} else {
							duplicateIndex[searchfor] = [ $(this) ];
						}
					}
				});

				// apply class: has-error to duplications
				for(var i in duplicateIndex) {
					if (duplicateIndex[i].length > 1) {
						for(var j in duplicateIndex[i]) {
							duplicateIndex[i][j].addClass('has-error');
						}
					} else {
						duplicateIndex[i][0].removeClass('has-error');
					}
				}

				return replacements;
			}

			function escapeRegExp(str) {
				return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
			}

			function updateSerializeStringLength(subject) {

				// Update serialized string sizes
				subject = subject.replace(/s\:(\d+)\:"([^"]+)"/g, function(match,p1,p2){ 
					// console.log(match,p1,p2);
					return 's:'+p2.length+':"'+p2+'"';
				});

				return subject;
			}

			function doUpdateOutput() {
				var subject = $('#source').val(),
					replacements = getReplacements();

				if ($('.replacements.has-error').length > 0) {
					$('#output').val( 'Unable to produce output, please fix "search for" conflict first.');
					return;
				}

				// Context is empty, no need to do anything
				if ( replacements.length == 0 ||  ! subject ) {
					$('#output').val( subject );
					return;
				}

				// Begin update
				var $alert = $('<div>')
						.css('margin-top', '10px')
						.addClass('alert alert-warning alert-md')
						.html('<strong><span class="glyphicon glyphicon-cog"></span> Working</strong> on your source string')
						.hide();
				$('#output').after( $alert );
				$alert.slideDown('fast');

				// Let's do real replacements
				var out = subject;
				for(var i in replacements) {
					var replacement = replacements[i],
						searchfor = replacement.searchfor,
						replacewith = replacement.replacewith;
	
					// Update subject
					out = out.replace(new RegExp(escapeRegExp(searchfor),'g'), replacewith);
				}
				out = updateSerializeStringLength(out);

				$('#output').val( out );

				// Update result notification
				setTimeout(function() {
					$alert
						.removeClass('alert-warning')
						.addClass('alert-success')
						.html( '<strong><span class="glyphicon glyphicon-check"></span> Replaced</strong> your output has been updated' );
				}, 700);

				// remove it
				setTimeout(function() {
					$alert.slideUp('medium', function() {
						$(this).remove();
					});
				}, 2400);
			}

			function shouldAddMoreReplacementBlock() {
				var replacements = getReplacements(),
					slotCount = $('.replacements').length,
					vacantSlots = slotCount - replacements.length,
					changed = false;

				if ( vacantSlots <= 0 ) {
					// Add 1 replacement block
					console.log("Adding new block");
					var cloned = replacementBlock.clone();
					cloned.hide();
					$('.replacements:last').after( cloned );
					cloned.slideDown('medium');

					changed = true;
				}

				if ( vacantSlots > 1 ) {
					var vacants = $('.replacements').filter(function() {
						return $.trim($(this).find('.searchfor').val()) == "";
					});

					// Remove everything except last one. (We are looping in reverse order)
					for(var i=vacants.length-2;i >= 0;i--) {
						$(vacants[i])
							.addClass('markasdelete')
							.slideUp('medium',function() {
								$(this).remove();
							});
					}
					changed = true;
				}

				if (changed) {
					// Update number in front.
					$('.replacements:not(.markasdelete)').each(function(i) {
						$(this).find('.indexbox').html('' + (i+1));
					});
				}
			}

			$(document).ready(function() {
				replacementBlock = $('.replacements').clone();
				$('#source').blur(doUpdateOutput);

				$(document).on('blur', '.replacewith,.searchfor', shouldAddMoreReplacementBlock);
			});
		</script>
	</body>
</html>